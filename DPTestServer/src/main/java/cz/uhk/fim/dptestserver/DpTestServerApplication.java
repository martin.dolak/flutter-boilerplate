package cz.uhk.fim.dptestserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DpTestServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DpTestServerApplication.class, args);
    }

}
