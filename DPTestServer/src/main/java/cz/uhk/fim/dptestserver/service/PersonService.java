package cz.uhk.fim.dptestserver.service;

import cz.uhk.fim.dptestserver.dto.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PersonService {

    private static final Map<Integer, Person> map = new HashMap<>();

    static {
        map.put(0, new Person("Martin", "martin@email.com"));
        map.put(1, new Person("Petr", "petr@email.com"));
        map.put(2, new Person("Marek", "marek@email.com"));
        map.put(3, new Person("Lukáš", "lukas@email.com"));
        map.put(4, new Person("David", "david@email.com"));
        map.put(5, new Person("Vojtěch", "vojtech@email.com"));
        map.put(6, new Person("Tereza", "tereza@email.com"));
        map.put(7, new Person("Tomáš", "tomas@email.com"));
        map.put(8, new Person("Jana", "jana@email.com"));
        map.put(9, new Person("Pavel", "pavel@email.com"));
        map.put(10, new Person("Michal", "michal@email.com"));
        map.put(11, new Person("Alena", "alena@email.com"));
        map.put(12, new Person("Klára", "klara@email.com"));
        map.put(13, new Person("Dagmar", "dagmar@email.com"));
        map.put(14, new Person("Jan", "jan@email.com"));
        map.put(15, new Person("Filipko", "filipko@email.com"));
        map.put(16, new Person("Zuzana", "zuzana@email.com"));
        map.put(17, new Person("Šárka", "sarka@email.com"));
        map.put(18, new Person("Miroslav", "miroslav@email.com"));
    }

    public List<Person> getPersons() {
        int size = map.size();

        List<Person> persons = new ArrayList<>();
        for (int i = 0; i < getRandomNo(size); i++) {
            int randomNo = getRandomNo(size);

            persons.add(map.get(randomNo));
        }
        return persons;
    }

    private static int getRandomNo(int size) {
        return (int) (Math.random() * (size - 1));
    }
}
