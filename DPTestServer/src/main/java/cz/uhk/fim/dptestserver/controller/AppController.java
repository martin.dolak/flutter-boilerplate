package cz.uhk.fim.dptestserver.controller;

import cz.uhk.fim.dptestserver.dto.Person;
import cz.uhk.fim.dptestserver.dto.ValidationError;
import cz.uhk.fim.dptestserver.service.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
public class AppController {

    private final PersonService personService;

    public AppController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/authorize")
    public ResponseEntity<Object> authorize() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/validation-error")
    public ResponseEntity<ValidationError> validationError() {
        ValidationError error = new ValidationError("Validation error from server", "error.validation.test");
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);

    }

    @GetMapping("/persons")
    public List<Person> getPersons() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        return personService.getPersons();
    }
}
