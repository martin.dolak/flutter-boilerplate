---
home: true
heroImage: ./fim-uhk-cs_hor.svg
tagline: Flutter boilerplate project
actionText: Začněme →
actionLink: /guide/
# features:
#   - title: Feature 1 Title
#     details: Feature 1 Description
#   - title: Feature 2 Title
#     details: Feature 2 Description
#   - title: Feature 3 Title
#     details: Feature 3 Description
footer: Made by Martin Dolák with ❤️
---
