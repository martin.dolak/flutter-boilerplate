# Notifikace

Pro správné fungování pluginu je nutné propojit aplikaci s řídícím panelem Firebase. Toto propojení je založeno na postupu, který je v řídícím panelu popsán pro každou platformu.

## Registrace aplikace ve Firebase

Registrace aplikace probíhá pro každou platformu zvlášť. Po založení projektu ve Firebase je umožněno registrovat na jednotlivé platformy.

### Android

V prvním kroku je důležité správně zadat ID aplikace (applicationId). To se nachází v projektu na cestě `android\app\build.gradle`. Toto **ID** může být určeno podle sebe a následně zadáno do formuláře konkrétně do políčka "Android package name". Idetifikátor se nachází v bloku `defaultConfig`, který je vyznačen na ukázce níže.

**Umístění ID aplikace**

```json{2}
    defaultConfig {
        applicationId "com.example.flutter_boilerplate"
        minSdkVersion lutter.minSdkVersion
        targetSdkVersion flutter.targetSdkVersion
        versionCode flutterVersionCode.toInteger()
        versionName flutterVersionName
    }
```

**Ukázka prvního kroku formuláře**

![firebase android step 1](./assets/ntf-step1.png)

V druhém kroku je nabídnut ke stažení soubor s názvem "google-service.json". Tento soubor musí být uložen do cesty `android\app`.

**Ukázka druhého kroku formuláře**

![firebase android step 2](./assets/ntf-step2.png)

Ve třetím kroku Firebase naviguje na umístění několika řádku kódu do příslušných souborů. Po dodržení daných instrukcí je projekt připraven na použití a je připraven na příjem notifikací.

**Ukázka třetího kroku formuláře**

![firebase android step 3](./assets/ntf-step3.png)

## Nastavení ve Flutteru

Pro Flutter je poskytován plugin Firebase Messaging ([firebase_messaging](https://pub.dev/packages/firebase_messaging).), které využívá služeb FCM a díky tomu je možné zpracovávat a zasílat push Notifikace na zařízení s operačním systémem Android a iOS.

**Přidání dependencies**

```bash
flutter pub add firebase_messaging
flutter pub add firebase_core
```

::: tip
Po přidání pluginu může být problém se spuštěním aplikace. Firebase messaging požaduje u Androidu minimální verzi SDK 19. Je tedy nutné tuto verzi zapsat do souboru `android\app\build.gradle`.

**Umístění ID aplikace**

```json{3}
    defaultConfig {
        applicationId "com.example.flutter_boilerplate"
        minSdkVersion 19
        targetSdkVersion flutter.targetSdkVersion
        versionCode flutterVersionCode.toInteger()
        versionName flutterVersionName
    }
```

:::

### Zpracování notifikací

Po předchozím propojení a přidání pluginu je aplikace schopna přijímat push zprávy. Pro zpracování těchto zpráv vystavuje plugin soubor tříd a metod, kterými lze zprávy obsluhovat. Plugin vystavuje rozhraní, kterým lze obsluhovat tři situace, ve kterých se mohou zprávy nacházet:

1.  Aplikace je otevřená a uživatel ji využívá.

```dart
    FirebaseMessaging.onMessage.listen((message) {
      // implement logic
    });
```

2.  Když je aplikace na pozadí, tedy není aktuálně využívána. Nebo je aplikace
    zcela vypnuta.

```dart
    FirebaseMessaging.onBackgroundMessage((message) {
      // implement logic
    });
```

::: warning UPOZORNĚNÍ
Pro zpracování by měla být využita top level funkce. Tedy funkce, která nevyžaduje např. inicializaci třídy.
:::

3.  Uživatel klikl na příchozí notifikaci a tím otevřel aplikaci.

```dart
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      // implement logic
    });
```

Za tímto účelem vzniká třída nazvaná `NotificationService`, jejímž úkolem je obstarávat příchozí push zprávy a zobrazovat příchozí informace uživateli. Třída by měla být zaváděna až na obrazovce, kdy je uživatel v části aplikace, kde dává smysl push notifikace přijímat. Tedy třídu není nutné zavádět hned po spuštění aplikace, ale např. při přihlášení uživatele apod. Za tímto účelem třída implementuje metodu `init`, která po jejím zavolání zavede potřebné prostředky důležité pro správné fungování push notifikací.

**Ukázka třídy `NotificationService`**

```dart
class NotificationService {
  FirebaseMessaging? _fm;

  void init() async {
    _fm = FirebaseMessaging.instance;

    await _fm?.getToken();

    FirebaseMessaging.onMessage.listen((message) {
      var notification = message.notification;
      if (notification == null) return;

      App.snackbarKey.currentState?.showSnackBar(
        SnackBar(
          content: Text(notification.title!),
          backgroundColor: Colors.orange,
          duration: const Duration(seconds: 5),
          behavior: SnackBarBehavior.floating,
        ),
      );
    });

    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      // implement logic
    });
  }
}
```

## Reference

[Firebase console](https://console.firebase.google.com/)

[Firebase messaging dokumentace](https://firebase.google.com/docs/cloud-messaging/flutter/receive)

[Plugin Firebase messaging](https://pub.dev/packages/firebase_messaging)
