# Úvod

Pro definované prvky vznikla dokumentace znázorňující možnou implementaci jednotlivých prvků. V dokumentaci jsou uvedeny příklady zavedení prvků doplněné o tipy, které byly při vývoji demonstrační aplikace zaznamenány. Dokumentace je také doplněna o ukázky kódu. Lze tedy dle dokumentace implementovat vlastní boilerplate.
