# Komunikace se serverem

Pokud aplikace potřebuje ukládat data online nebo získávat data z jiných zdrojů než lokálních, je potřeba komunikovat s určitým serverem. Za tímto účelem je vhodné využít knihovny, která tuto komunikaci zprostředkovává. Jednou z nich je knihovna Dio ([Knihovna Dio](https://pub.dev/packages/dio)).

## Knihovna Dio

Jedná se o knihovnu, která zprostředkovává komunikaci klient-server založenou na využití prostředků protokolu HTTP. Pro tento účel knihovna Dio vystavuje třídy a metody zjednodušující tuto práci. Knihovna umožňuje pokročilé zpracování požadavků, podporuje obecnou obsluhu požadavků pomocí tzv. interceptorů, zamítání požadavků, stahování souborů, nastavení časových limitů požadavků a mnoho dalších

**Instalace dependencies**

```bash
flutter pub add dio
```

Knihovnu lze využívat hned od počátku instalace. Není potřeba nic nastavovat a knihovnu hned využívat pro komunikaci.

**Volání metody get()**

```dart
Dio().get("https://server.com/test")
```

::: tip
Při použití lokálního serveru - localhost:XXXX nemusí Android emulátor vidět Váš server. Lze tedy pro správné fungování využít IP adresy serveru, typicky - **http://10.0.2.2:XXXX**
:::

Avšak tento přístup není zcela úplný, pokud aplikace potřebuje před odesláním požadavek zpracovat nebo nastavit globální parametry chování. Muselo by se tedy vzniklému objektu vždy předávat požadované nastavení, což by vedlo k výraznému opakování již napsaného kódu. Pro tento účel knihovna poskytuje třídy, které lze dědit a díky tomu si vytvořit vlastního komunikačního klienta s potřebným nastavením. Takový klient je pak spravován na jednom místě a není nutné ho udržovat na více místech.

**Globální klient pro celou aplikaci**

```dart
class AppHttpClint extends DioForNative {

  static AppHttpClint createClient() {
    return AppHttpClint()
      ..options.connectTimeout = AppConfig.REQUEST_CONNECTION_TIMEOUT
      ..options.receiveTimeout = AppConfig.REQUEST_RECEIVE_TIMEOUT
      ..options.baseUrl = FlavorConfigHolder.inst.serverUrl
      ..interceptors.addAll(
        [],
      );
  }
}
```

**Globální klient pro celou aplikaci s využitím knihovny GetIt**

```dart
@singleton
class AppHttpClint extends DioForNative {
  @factoryMethod
  static AppHttpClint createClient() {
    // Client settings
  }
}
```

## Reference

[Knihovna Dio](https://pub.dev/packages/dio)

[Knihovna GetIt](https://pub.dev/packages/get_it)
