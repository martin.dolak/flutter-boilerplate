# Vnitřní stav aplikace

Stavové widgety udržují svůj vnitřní stav. Takové widgety zobrazují v aplikaci data, která mohou být modifikována např. pomocí ovládacích prvků UI. Takto modifikovatelná data mohou být zobrazena ve více widgetech a je u nich očekáváno, že budou na všech zobrazených místech konzistentní. Je tedy vhodné taková data logicky spravovat tak, aby byla konzistentní pro všechny widgety, které s nimi pracují. V aplikaci, která obsahuje více widgetů, se může tato správa stát komplikovanou a je vhodné využít nástrojů určených pro tento účel. Jedním z nich je knihovna Bloc.

## Knihovna Bloc

Bloc je populární architektonický vzor využívaný při vývoji aplikací. Je využit zejména k návrhu a vývoji aplikací. Bloc využívá konceptu reaktivního programování. V tomto konceptu je využito proudů dat (streamů). V něm proudí různé události, které jsou odposlouchávány. Odběratelé tohoto streamu získávají informace o událostech a mohou na ně patřičně reagovat. Widgety ve Flutteru používají tento stream pro komunikaci a odesílání dat. Díky těmto streamům je možné oddělit obchodní logiku od UI, takže je samotný kód udržitelnější a čitelnější. Návrhový vzor Bloc přináší dva koncepty, které jsou využívány pro správu stavu aplikace. Tyto koncepty nazýváme `Cubit` a `Bloc`.

**Architektura vzoru Bloc**

![Architektura vzoru Bloc](./assets/blocArchitecture.drawio.png)

### Použití

Před použitím tříd knihovny Bloc je důležité zavést třídy do contextu aplikace k tomuto slouží třída `BlocProvider`. Po tomto vložení je objekt `Bloc` dostupný v aplikaci a lze s ním pracovat v uživatelském rozhraní aplikace.

**Ukázka třídy `BlocProvider`**

```dart
BlocProvider<TestDataBloc>(
        create: (context) => TestDataBloc(),
        child: const Body(),
      )

// Vložení objektu pomocí knihovny GetIt
BlocProvider<TestDataBloc>(
        create: (context) => getIt<TestDataBloc>(),
        child: const Body(),
      )
```

Pro využití knihovny GetIt je důležité založenou třídu `Bloc` nebo `Cubit` anotovat. Pro tento účel knihovna GetIt definuje anotaci `@injectable`. Díky tomu je možné do konstruktoru třídy předávat další pomocné třídy využívající knihovny GetIt.

**Příklad třídy Bloc s využitím knihovny GetIt**

```dart
@injectable
class TestDataBloc extends Bloc<TestDataEvent, TestDataState> {
  final TestDataRepository _testDataRepository;

  TestDataBloc({
    required TestDataRepository testDataRepository,
  })  : _testDataRepository = testDataRepository,
        super(const TestDataState.initial()) {
    on<Refresh>((event, emit) async {
      emit(const TestDataState.loading());

      try {
        await _testDataRepository.refreshData();
      } catch (e) {
        addError(e);
        emit(const TestDataState.refreshed());
      }

      emit(const TestDataState.refreshed());
    });
  }
}
```

Jelikož každá třída `Bloc` vyžaduje další dvě třídy pro své fungování, je vhodné využít knihovny freezed. Díky této knihovně lze zjednodušit zápis potřebných tříd - tříd stavu a událostí. V případě třídy `Cubit` je potřebná pouze třída stavu.

**Ukázka stavové třídy vyuívajicí knihovny Freezed**

```dart
@freezed
class TestDataState with _$TestDataState {
  const factory TestDataState.initial() = _Initial;
  const factory TestDataState.refreshed() = Refreshed;
  const factory TestDataState.loading() = Loading;
}
```

## Reference

[Flutter Bloc](https://pub.dev/packages/flutter_bloc)

[Knihovna Freezed](https://pub.dev/packages/freezed)

[Knihovna GetIt](https://pub.dev/packages/get_it)
