# Prostředí (Flavors)

Mnoho systémů potřebuje ve svých aplikacích zavádět různá prostředí (typicky nazvané **environments**, **flavors**). Prostředí jsou zaváděna pro různé úrovně vývoje, mezi typické názvy prostředí patří:

- **dev** - development (vývoj),
- **test** - test,
- **prod** - produkce.

Důvodem zavádění prostředí je potřeba načítání hodnot definovaných pro jednotlivá prostředí. Tyto hodnoty slouží zejména pro konfiguraci, kde díky nim aplikace např. komunikuje s jinou aplikací, která je typicky definována pro dané prostředí. Příkladem může být mobilní aplikace, která je nastavená na prostředí **test**, která má za cíl komunikovat se serverem, který je taky nastaven v prostředí **test**.

## Založení prostředí

V aplikaci jsou řešeny **dva druhy** prostředí. První prostředí je prostředí přímo v aplikaci, které obsahuje definovanou konfiguraci. A druhé prostředí je prostředí dané platformy (iOS, Android), díky kterému pak vznikají aplikace pro každé definované prostředí zvlášť.

Každé z definovaných prostředí vyžaduje založení nové spustitelné třídy, která volá metodu `runApp()` definouvanou ve Flutter balíčku `flutter/material.dart`.

::: tip
Soubor je vhodné pojmenovat tak, aby bylo jasné, k jakému z definovaných prostředí soubor patří. Přídem pro prostředí **dev** může být název **main-dev.dart**.
:::

Aby se dala jednotlivá prostředí v aplikaci konfigurovat, vzniká třída s názvem `FlavorConfigHolder`, která obsahuje základní atributy pro konfiguraci, které je možné jednoduše rozšířit. Do každého z vytvořených spustitelných souborů pro definovaná prostředí je vhodné tuto třídu zavést. Zavedená třída je vytvořena při spuštění aplikace a bude tedy dostupná globálně pro celou aplikaci v každém okamžiku jejího běhu.

**Vytvoření instance konfigurace pro prostředí**

```dart
  FlavorConfigHolder.create(
    flavor: FLAVOR.dev,
    serverUrl: AppConfig.DEV_SERVER_BASE_URL,
    isProd: false,
  );
```

**Přístup k globální instanci v aplikaci**

```dart
FlavorConfigHolder.inst // přístup k instanci

FlavorConfigHolder.inst.flavorName // volání metod na instanci
```

Vytvoření prostředí pro platformu Android a iOS se musí definovat zvlášť. Jak již bylo zmíněno výše, je toto potřebné pokud je cílem získat více instancí aplikace pro každé definované prostředí (Pro každé prostředí je v zařízení nainstalována "jiná" aplikace).

### Android

Pro Android prostředí je nutné do konfigurace zavést infomace o jednotlivých prostředí. Konfigurační soubor se nachází na cestě `android\app\build.gradle`, kde prostředí zapisujeme do bloku s názvem `android`.

**Konfirguracce pro prostředí dev, prod**

```json
    flavorDimensions "default"
    productFlavors {
        prod {
            dimension "default"
            resValue "string", "app_name", "Prod app" // definice hodnoty obsahující název aplikace
            applicationIdSuffix ""
        }
        dev {
            dimension "default"
            resValue "string", "app_name", "Dev app" // definice hodnoty obsahující název aplikace
            applicationIdSuffix ".dev"
        }
    }
```

### iOS

::: warning UPOZORNĚNÍ
Autor nedisponuje apple zařízením, které je pro tuto konfiguraci nutné.
:::

### Spuštění aplikace v požadovaném prostředí

Aby byla aplikace spuštěna ve správném prostředí je nutné při spouštění aplikace předat vhodné argumenty, dle kterých se na dané platformě spustí definované prostředí.

**Skript pro spuštění**

```bash
flutter run --target lib/main_dev.dart --flavor dev
```

::: tip
Jelikož je nutné tento příkaz spustit vždy při novém spuštění aplikace je dobré si nakonfigurovat spouštěcí konfiguraci, která za nás spustí příkaz bez nutnosti jeho zápisu.
:::

Nastavení spouštěcí konfigurace je odlišné pro každé IDE, ve kterém je aplikace vyvíjena.

### Visual Studio Code (VS CODE)

Do kořenového adresáře našeho projektu přidáme soubor `launch.json` do souboru `.vscode`.

**Ukázka konfigurace**

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Dev Launch",
      "request": "launch",
      "type": "dart",
      "program": "lib/main_dev.dart",
      "args": ["--flavor", "dev", "--target", "lib/main_dev.dart"]
    }
  ]
}
```

## Název aplikace dle prostředí

Pro odlišení jednotlivých nainstalovaných apliakcí je možné nastavit každé z nich jiný název. Pro **dev** prostředí např. _Aplikace (dev)_, pro **prod** prostředí např. _Aplikace_.

### Android

Při [konfiguraci jednotlivých prostředí](./flavors.html#android) je potřebné přidat parametr názvu aplikace. Toho lze docílit zavedením `resValue`.

```json
resValue "string", "app_name", "Aplikace" //prod

resValue "string", "app_name", "Aplikace (dev)" //dev
```

K této hodnotě pak můžeme přistoupit a tím definovat název aplikace pro každé prostředí. Název aplikace je definován v souboru `AndroidManifest.xml`, který se nachází na cestě `android\app\src\main`. Do tohoto souboru přidámě `android:label="@string/app_name"`, címž docílíme změny názvu, dle námi definované hodnoty.

**Ukázka změny názvu**

```xml{3}
<manifest>
   <application
        android:label="@string/app_name"
        android:name="${applicationName}">
        </activity>
        ...
```

::: warning UPOZORNĚNÍ
Soubor `AndroidManifest.xml` je v ukázce ořezán o nepotřebné detaily, ve svém projektu je nutné pouze přidat `android:label="@string/app_name"` na vyznačené místo.
:::

### iOS

::: warning UPOZORNĚNÍ
Autor nedisponuje apple zařízením, které je pro tuto konfiguraci nutné.
:::

## Ikony aplikace dle prostředí

Dalším způsobem jak nainstalované aplikace pro jednotlivá prostředí odlišit je změna jejich ikon. Pro tento úkol je vhodné využít knihovnu [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons), která se postará o správné vygenerování ikon pro jednotlivá prostředí.

Pro každé prostředí je nutné vytvořit soubor s definovaným názvem `flutter_launcher_icons-FLAVOR.yaml`. Tedy pro příklad, **dev** prostředí ponese název `flutter_launcher_icons-dev.yaml`. Všechny soubory jsou uloženy v kořenovém adresáři aplikace.

**flutter_launcher_icons-dev.yaml**

```yaml
flutter_icons:
  android: true # generování ikon pro Android
  ios: true # generování ikon pro iOS
  image_path: "assets/app_icons/dev_icon.png" # cesta k ikoně
  remove_alpha_ios: true # iOS nepodporuje průhlednost u ikon, tedy je vhodné automaticky odstranit
```

::: warning UPOZORNĚNÍ
Je nutné mít ikony ve formátu **1024x1024**.
:::

Pokud jsou soubory pro každé prostředí nakonfigurovány, knihovna nabízí příkaz, pomocí kterého automaticky vygeneruje ikony pro jednotlivá prostředí.

**Generování ikon**

```bash
flutter pub run flutter_launcher_icons:main -f flutter_launcher_icons*
```

::: tip
Pokud je používán Windows CMD, je možné, že příkaz skončí chybou. Vhodné je namísto CMD použít např. **Git Bash**.
:::

Více informací, jak lze generovat ikony lze nalézt v oficiální dokumentaci [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons).
