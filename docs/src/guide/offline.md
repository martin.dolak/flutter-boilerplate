# Provoz aplikace offline

Aby aplikace mohla svým uživatelům poskytovat obsah i když uživatel není připojen k internetu je důležité využít lokálních zdrojů, které aplikace má. Pro ukládání dat, jenž byla dříve načtena, je možné využít lokální databáze. Pro tyto účely existuje mnnoho knihoven a jednou z nic je knihovna Hive.

## Knihovna Hive

Hive ([Knihovna Hive](https://pub.dev/packages/hive)) je velmi rychlou lokální databází založenou na principu klíč hodnota. Jedná se o velmi jednoduchý princip, kde ke každému klíči je přidělena hodnota v databázi. Celá knihovna je napsána v jazyku Dart

**Instalace dependencies**

```bash
flutter pub add hive
flutter pub add hive_flutter
flutter pub add hive_generator
```

Knihovnu je důležité před samotným spuštěním aplikace inicializovat.

**Inicializace knihovny**

```dart
Hive.initFlutter()
```

::: warning UPOZORNĚNÍ
Voláno typicky v `main` metodě před spuštění aplikace pomocí Flutter metody `runApp`
:::

Data určenáá pro offline přístup jsou v knihovně ukládána do objektu třídy `Box`. Tuto třídu je důležité zavést také před spuštěním, aby byla dostupá ppo celou dobu životnosti aplikace.

**Zavedení a přístup k objektu třídy `Box`**

```dart
Hive.openBox<T>("boxName"); // Zavedení objektu

Hive.box<T>("boxName"); // přístup k objektu
```

Samotný přístup k objektu lze sjednotit pro celou aplikaci. K tomu musíme využít knihovnu GetIt, která zpřístupní objekt v celé aplikaci a je možné ho automaticky inicializovat do konstruktorů třídy využívajících také knihovny GetIt.

**Zpřístupení objektu třídy `Box` pomocí knihovny GetIt**

```dart
@module
abstract class DbModule {
  @singleton
  @Named("boxName")
  Box get testDataBox => Hive.box<Person>("boxName");
}
```

Aby byla offline data zpracovávána z jednoho místa v kódu, je vhodné zavádět samostatné třídy pro toto zpracování. Pokud třída zpracování využívá knihovny GetIt lze objekt typu `Box` inicializovat pomocí konstruktoru. Pokud třídy knihovny nevyužívá, mouhou být data zpřístupněna pomocí volání předchozí metody `Hive.box<T>("boxName")`.

**Ukázka třídy zpracovávající offline data**

```dart
@lazySingleton
class AppTestDataStorage {
  final Box _dataStorage;

  AppTestDataStorage(
    @Named(Constants.APP_TEST_DATA_BOX) final Box dataStorage,
  ) : _dataStorage = dataStorage;

  Future<void> refreshPersons(List<Person> persons) async {
    await _dataStorage.clear();
    await _dataStorage.addAll(persons);
  }
}
```

## Reference

[Knihovna Hive](https://pub.dev/packages/hive)

[Dokumentace Hive](https://docs.hivedb.dev/)

[Knihovna GetIt](https://pub.dev/packages/get_it)
