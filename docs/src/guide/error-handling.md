# Zpracování vyjímek

Zpracování chyb ve Flutteru je rozděleno na dva způsoby. V prvním Flutter zachytává chyby způsobeny během volání v rámci samotného Flutteru včetně chyb, ke kterým dochází během fáze sestavování, rozvržení a vykreslování widgetů. Druhým případem jsou chyby, které nejsou vyhozeny v rámci volání ve Flutteru, tyto chyby nejsou zachytávány, ale mohou být obslouženy nastavením třídy `Zone`.

Všechny chyby zachycené Flutterem jsou přesměrovány a obslouženy pomocí metody `FlutterError.onError`, která v základním nastavení zobrazuje chyby do logu pomocí volání metody `FlutterError.presentError`. Toto logování je vhodné zachovat, jelikož díky němu je vývojář informován z jakého důvodu byla chyba vyhozena.

**Zachycení vyjímek v aplikaci**

```dart
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();

    FlutterError.onError = (details) {
      ErrorHandler.handleThrownException(details); // Zachycení Flutter vyjímek
    };

    runApp(const App());
  }, (error, stack) {
    ErrorHandler.handleThirdPartyException(error, stack); // Zpracování vyjímek pomocí třídy Zone
  });
```

## Zpracování Dio výjimek

Knihovna Dio v případě speciálních událostí, které je nutné v aplikaci obstarat, vyhazuje svou vlastní výjimku třídy `DioError`, která obsahuje podrobné informace o dané výjimce. Lze tyto vyjímky transformovat na vyjímky aplikace, aby bylo snadnější tyto výjimky obstarávat.

**Třída definující výjimky aplikace**

```dart
@freezed
class AppError with _$AppError implements Exception {
  const factory AppError.validationError(ValidationResult? result) =
      ValidationError;
  const factory AppError.uknownError(String? message) = UnknownError;
  const factory AppError.serverError() = ServerError;
  const factory AppError.notFoundError() = NotFoundError;
  const factory AppError.timeoutExceededError() = TimeoutExceededError;
  const factory AppError.badRequestError() = BadRequestError;
}
```

Takto zpracované výjimky lze obsloužit v třídě `ErrorHandler`, kde je možné s výjimkami proacovat jak je potřeba.

**Příklad zpracování výjimek**

```dart
  static AppError handleDioError(DioError error) {
    switch (error.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw const TimeoutExceededError();
      case DioErrorType.other:
      case DioErrorType.cancel:
        break;
      case DioErrorType.response:
        switch (error.response?.statusCode) {
          case 404:
            throw const NotFoundError();
          case 500:
            throw const ServerError();
          case 400:
            var result = ValidationResult.fromJson(error.response?.data);
            return ValidationError(result);
        }
        break;
    }
    throw UnknownError(error.message);
  }
```

## Zpracování Bloc výjimek

Knihovna Bloc umožňuje vyvolání vlastních výjimek. Pro tento případ jsou třídy `Bloc` a `Cubit` vybaveny metodou `addError`. Na toto přidání výjimky lze reagovat přímo v příslušné třídě přepsáním metody onError . Avšak toto zpracování výjimky by se mohlo týkat pouze dané třídy, která výjimku vyhazuje. Pro obecné zachytávání těchto výjimek knihovna Bloc přináší třídu `BlocObserver`, která mimo jiné může reagovat na výjimky vyvolané zmiňovanou metodou addError . Toto zpracování výjimek je pak obecné pro všechny třídy, které dědí z tříd `Bloc` nebo `Cubit`. Lze tedy dostat všechny vyjímky knihovny Bloc na jedno místo a zpracovat je již zmiňovanou třídou `ErrorHandler`.

**Příklad využití třídy `BlocObserver`**

```dart
class AppBlocObserver extends BlocObserver {
  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    ErrorHandler.handleThirdPartyException(error, stackTrace);
  }
}
```

::: warning UPOZORNĚNÍ
Vzniklou třídu `AppBlocObserver` je nutné zanést do knihovny Bloc, aby plnila svůj účel, pro který byla vytvořena. Toto přiřazení by mělo být vykonáno před spuštěním samotné aplikace.

```dart
Bloc.observer = AppBlocObserver();
```

:::

## Reference

[Handling errors in Flutter](https://docs.flutter.dev/testing/errors)

[Bloc Observer](https://bloclibrary.dev/#/coreconcepts?id=blocobserver)

[Dio Error](https://pub.dev/packages/dio#handling-errors)

[Knihovna Freezed](https://pub.dev/packages/freezed)
