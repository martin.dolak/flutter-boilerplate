# Udržitelný styl aplikace

Aby aplikace působila konzistentně z hlediska stylu, je důležité tento styl udržovat. Flutter pro tento problém definuje třídu, která se stará o definici tzv. motivů, které v sobě nesou vývojářem definovanou informaci o stylu aplikace. Tyto motivy sdílejí barvy, styly písma a chování napříč celou aplikací. Lze tedy definovat motivy pro celou aplikace, ale i pouze pro její části. Za tímto účelem je vytvořena třída `Theme`, která v sobě nese zmiňovanou definici barev, stylu textů apod. Takto definovanou třídu pak stačí zanést do konstruktoru třídy `MaterialApp`, která slouží jako základ pro celou aplikaci.

```dart
MaterialApp(
  theme: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blue,
    textTheme: const TextTheme(
      headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
    ),
  ),
  home: const HomeScreen(),
);
```

## Rozložení dle velikosti obrazovky

Jelikož zařízení disponují jinými velikostmi jejich obrazovky, je důležité se zaměřit i na rozložení zobrazovaného obsahu. Pokud je cílem, aby aplikace byla spustitelná na více zařízeních, než je zařízení mobilní, je rozložení zobrazení velmi důležité. Mohlo by dojít ke ztrátě informace, která má být koncovému uživateli předána.

Za tímto účelem Flutter přináší třídu `LayoutBuilder`, která vrací widgety podle předem stanovených podmínek. Typicky se podmínky týkají velikosti obrazovky zařízení. Není vždy důležité využít této třídy, jelikož některé návrhy zobrazení nevytváří problémy na způsobené velikostí obrazovky. Tuto třídu je tedy výhodné využívat ve chvíli, kdy je nutné celou obrazovku definovat pro různé velikosti znovu.

::: tip
Podmínky pro velikosti obrazovky lze definovat jako přídavné metody na `BuildContext`. Ten je dostupný v každém vykreslovaném widgetu, takže se jeví jako ideální místo pro definici těchto podmínek.

**Ukázka definování podmínek velikosti obrazovky**

```dart
extension BuildContextExt on BuildContext {
  bool isMobile() =>
      MediaQuery.of(this).size.shortestSide <=
            AppConfig.MOBILE_WIDTH_MAX_SIZE;

  bool isTablet() {
    var shortestSide = MediaQuery.of(this).size.shortestSide;
    return shortestSide > AppConfig.MOBILE_WIDTH_MAX_SIZE &&
        shortestSide <= AppConfig.TABLET_WIDTH_MAX_SIZE;
  }

  bool isDesktop() =>
      MediaQuery.of(this).size.shortestSide >
            AppConfig.TABLET_WIDTH_MAX_SIZE;
}
```

:::

Pokud je cílem zobrazit jiné rozložení pro různé velikosti obrazovky, je vytvořena třída `ResponsiveBuilder`. Tato třída kombinuje využití třídy `LayoutBuilder` a definovaných podmínek.

**Ukázka třídy `ResponsiveBuilder`**

```dart
class ResponsiveBuilder extends StatelessWidget {
  final Widget mobile;
  final Widget? tablet;
  final Widget? desktop;
  // Constructor

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (context.isMobile()) return mobile;
        if (context.isTablet()) return tablet ?? mobile;
        return desktop ?? tablet ?? mobile;
      },
    );
  }
}
```

## Změna theme za běhu aplikace

Na začátku je důležité definovat třídy `theme`, mezi kterými je požadováno měnit styl aplikace.

**Ukázka definovaných tříd `theme`**

```dart

// Light theme
ThemeData lightTheme() {
  return ThemeData(
    brightness: Brightness.light,
    appBarTheme: const AppBarTheme(
      color: AppColors.LIGHT_PRIMARY_COLOR,
    ),
    primaryColor: AppColors.LIGHT_PRIMARY_COLOR,
    errorColor: Colors.red,
    scaffoldBackgroundColor: Colors.white,
    colorScheme: const ColorScheme.light(
      secondary: Colors.orange,
    ),
    backgroundColor: Colors.white,
  );
}

// Dark theme
ThemeData darkTheme() {
  return ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColor: AppColors.DARK_PRIMARY_COLOR,
    errorColor: Colors.red,
    scaffoldBackgroundColor: Colors.grey.shade700,
    appBarTheme: const AppBarTheme(
      color: AppColors.DARK_PRIMARY_COLOR,
    ),
    colorScheme: ColorScheme.dark(
      secondary: Colors.lime.shade400,
    ),
    backgroundColor: Colors.grey.shade700,
  );
}
```

Po této definici je možné využít knihovny Bloc. Ta může udržovat aktuální stav vybraného `theme` a vystavovat metody, které umožné tento stav změnit.

**Příklad třídy měnící theme aplikace**

```dart
@injectable
class DesignCubit extends Cubit<DesignState> {
  DesignCubit() : super(const DesignState.light()); // Init state

  // Method for theme change
  void changeTheme() {
    emit(state.when(
      light: () => const DesignState.dark(),
      dark: () => const DesignState.light(),
    ));
  }
}
```

Následně stačí celou aplikaci obalit třídou `BlocBuilder` a poslouchat změny stavu a příslušně na ně reagovat.

**Ukázka použití třídy `BlocBuilder`**

```dart
    return BlocBuilder<DesignCubit, DesignState>(
      builder: (context, state) => MaterialApp(
        ...
        theme: state.when(
          light: () => lightTheme(),
          dark: () => darkTheme(),
        ),
        themeMode: ThemeMode.system,
        ...
      ),
    );
```

## Reference

[Flutter theme](https://docs.flutter.dev/cookbook/design/themes)

[Flutter responsive](https://docs.flutter.dev/development/ui/layout/adaptive-responsive)

[Flutter Bloc](https://pub.dev/packages/flutter_bloc)

[Knihovna GetIt](https://pub.dev/packages/get_it)
