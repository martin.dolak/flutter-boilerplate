import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/presentation/app.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class NotificationService {
  FirebaseMessaging? _fm;

  void init() async {
    _fm = FirebaseMessaging.instance;

    await _fm?.getToken();

    FirebaseMessaging.onMessage.listen((message) {
      var notification = message.notification;
      if (notification == null) return;

      App.snackbarKey.currentState?.showSnackBar(
        SnackBar(
          content: Text(notification.title!),
          backgroundColor: Colors.orange,
          duration: const Duration(seconds: 5),
          behavior: SnackBarBehavior.floating,
        ),
      );
    });

    FirebaseMessaging.onMessageOpenedApp.listen((event) {
      // implement logic
    });
  }
}
