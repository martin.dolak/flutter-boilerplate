import 'package:dio/dio.dart';
import 'package:flutter_boilerplate/config/common/error/error.dart';
import 'package:flutter_boilerplate/config/handler/error_handler.dart';
import 'package:flutter_boilerplate/config/http/app_http_client.dart';
import 'package:flutter_boilerplate/config/http/result.dart';
import 'package:injectable/injectable.dart';

abstract class AuthRepository {
  Future<void> authorize();
  Future<Result<dynamic, AppError>> validatioError();
}

@Singleton(as: AuthRepository)
class AuthRepositoryImpl extends AuthRepository {
  final AppHttpClint _http;

  AuthRepositoryImpl({required AppHttpClint http}) : _http = http;

  @override
  Future<void> authorize() async {
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  Future<Result<dynamic, AppError>> validatioError() async {
    try {
      var response = await _http.get("/validation-error");
      return Result.success(response.data);
    } on DioError catch (e) {
      return Result.error(ErrorHandler.handleDioError(e));
    }
  }
}
