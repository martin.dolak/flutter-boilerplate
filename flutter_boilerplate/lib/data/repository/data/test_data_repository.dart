
import 'package:dio/dio.dart';
import 'package:flutter_boilerplate/config/handler/error_handler.dart';
import 'package:flutter_boilerplate/config/http/app_http_client.dart';
import 'package:flutter_boilerplate/data/model/data/person.dart';
import 'package:flutter_boilerplate/db/app_test_data_storage.dart';
import 'package:injectable/injectable.dart';

abstract class TestDataRepository {
  Future<void> refreshData();
}

@LazySingleton(as: TestDataRepository)
class TestDataRepositoryImpl extends TestDataRepository {
  final AppHttpClint _http;
  final AppTestDataStorage _storage;

  TestDataRepositoryImpl({
    required AppHttpClint http,
    required AppTestDataStorage storage,
  })  : _http = http,
        _storage = storage;

  @override
  Future<void> refreshData() async {
    Response response;
    try {
      response = await _http.get("/persons");
    } on DioError catch (e) {
      ErrorHandler.handleDioError(e);
      return;
    }

    var persons =
        List<Person>.from(response.data.map((e) => Person.fromJson(e)));

    await _storage.refreshPersons(persons);
  }
}
