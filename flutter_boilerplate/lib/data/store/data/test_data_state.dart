part of 'test_data_bloc.dart';

@freezed
class TestDataState with _$TestDataState {
  const factory TestDataState.initial() = _Initial;
  const factory TestDataState.refreshed() = Refreshed;
  const factory TestDataState.loading() = Loading;
}
