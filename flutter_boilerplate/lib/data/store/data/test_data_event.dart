part of 'test_data_bloc.dart';

@freezed
class TestDataEvent with _$TestDataEvent {
  const factory TestDataEvent.refresh() = Refresh;
}
