import 'package:bloc/bloc.dart';
import 'package:flutter_boilerplate/data/repository/data/test_data_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'test_data_event.dart';
part 'test_data_state.dart';
part 'test_data_bloc.freezed.dart';

@injectable
class TestDataBloc extends Bloc<TestDataEvent, TestDataState> {
  final TestDataRepository _testDataRepository;

  TestDataBloc({
    required TestDataRepository testDataRepository,
  })  : _testDataRepository = testDataRepository,
        super(const TestDataState.initial()) {
    on<Refresh>((event, emit) async {
      emit(const TestDataState.loading());

      try {
        await _testDataRepository.refreshData();
      } catch (e) {
        addError(e);
        emit(const TestDataState.refreshed());
      }

      emit(const TestDataState.refreshed());
    });
  }
}
