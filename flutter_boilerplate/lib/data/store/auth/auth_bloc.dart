import 'package:bloc/bloc.dart';
import 'package:flutter_boilerplate/config/common/error/error.dart';
import 'package:flutter_boilerplate/data/repository/auth/auth_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;

  AuthBloc({
    required AuthRepository authRepository,
  })  : _authRepository = authRepository,
        super(const AuthState.init()) {
    on<Started>((event, emit) async {
      await _authRepository.authorize();
      emit(const AuthState.authorized());
    });
    on<ValidationError>((event, emit) async {
      var result = await _authRepository.validatioError();

      result.when(
        success: (_) => {},
        error: (e) => emit(AuthState.error(e)),
      );
    });
  }
}
