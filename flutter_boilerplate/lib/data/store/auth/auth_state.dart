part of 'auth_bloc.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState.init() = _Init;
  const factory AuthState.authorized() = Authorized;
  const factory AuthState.error(AppError error) = Error;
}
