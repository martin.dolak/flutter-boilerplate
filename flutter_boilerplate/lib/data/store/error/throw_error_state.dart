part of 'throw_error_cubit.dart';

@freezed
class ThrowErrorState with _$ThrowErrorState {
  const factory ThrowErrorState.init() = _Init;
  const factory ThrowErrorState.error(String message) = Error;
}
