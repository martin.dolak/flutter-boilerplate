import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'throw_error_state.dart';
part 'throw_error_cubit.freezed.dart';

@injectable
class ThrowErrorCubit extends Cubit<ThrowErrorState> {
  ThrowErrorCubit() : super(const ThrowErrorState.init());

  void throwException() {
    emit(const ThrowErrorState.init());
    throw Exception("Exception is thrown from Cubit");
  }

  void handleException() {
    try {
      throw Exception("Exception handled in Cubit");
    } catch (e) {
      emit(ThrowErrorState.error(e.toString()));
    }
  }

  void addException() {
    addError(Exception("Exception added from Cubit"));
  }
}
