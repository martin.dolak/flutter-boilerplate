part of 'design_cubit.dart';

@freezed
class DesignState with _$DesignState {
  const factory DesignState.light() = Light;
  const factory DesignState.dark() = Dark;
}
