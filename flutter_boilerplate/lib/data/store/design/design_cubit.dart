import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'design_state.dart';
part 'design_cubit.freezed.dart';

@injectable
class DesignCubit extends Cubit<DesignState> {
  DesignCubit() : super(const DesignState.light());

  void changeTheme() {
    emit(state.when(
      light: () => const DesignState.dark(),
      dark: () => const DesignState.light(),
    ));
  }
}
