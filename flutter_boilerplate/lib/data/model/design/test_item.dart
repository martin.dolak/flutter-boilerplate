import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'test_item.freezed.dart';

@freezed
class TestItem with _$TestItem {
  const factory TestItem({
    required String label,
    required IconData icon,
  }) = _TestItem;
}

const List<TestItem> list = [
  TestItem(label: "UHK Fim", icon: Icons.school),
  TestItem(label: "Flutter", icon: Icons.flutter_dash),
  TestItem(label: "Food", icon: Icons.no_meals),
  TestItem(label: "Books", icon: Icons.book),
  TestItem(label: "Person", icon: Icons.person),
  TestItem(label: "Alarm", icon: Icons.access_alarm),
  TestItem(label: "Account balance", icon: Icons.account_balance),
  TestItem(label: "Android", icon: Icons.android),
  TestItem(label: "Edit", icon: Icons.edit),
  TestItem(label: "Cancel", icon: Icons.cancel),
  TestItem(label: "Moped", icon: Icons.moped),
  TestItem(label: "Car", icon: Icons.car_rental),
  TestItem(label: "Shopping basket", icon: Icons.shopping_basket),
];
