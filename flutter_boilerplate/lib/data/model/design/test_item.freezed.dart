// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TestItem {
  String get label => throw _privateConstructorUsedError;
  IconData get icon => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TestItemCopyWith<TestItem> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestItemCopyWith<$Res> {
  factory $TestItemCopyWith(TestItem value, $Res Function(TestItem) then) =
      _$TestItemCopyWithImpl<$Res>;
  $Res call({String label, IconData icon});
}

/// @nodoc
class _$TestItemCopyWithImpl<$Res> implements $TestItemCopyWith<$Res> {
  _$TestItemCopyWithImpl(this._value, this._then);

  final TestItem _value;
  // ignore: unused_field
  final $Res Function(TestItem) _then;

  @override
  $Res call({
    Object? label = freezed,
    Object? icon = freezed,
  }) {
    return _then(_value.copyWith(
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
      icon: icon == freezed
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as IconData,
    ));
  }
}

/// @nodoc
abstract class _$$_TestItemCopyWith<$Res> implements $TestItemCopyWith<$Res> {
  factory _$$_TestItemCopyWith(
          _$_TestItem value, $Res Function(_$_TestItem) then) =
      __$$_TestItemCopyWithImpl<$Res>;
  @override
  $Res call({String label, IconData icon});
}

/// @nodoc
class __$$_TestItemCopyWithImpl<$Res> extends _$TestItemCopyWithImpl<$Res>
    implements _$$_TestItemCopyWith<$Res> {
  __$$_TestItemCopyWithImpl(
      _$_TestItem _value, $Res Function(_$_TestItem) _then)
      : super(_value, (v) => _then(v as _$_TestItem));

  @override
  _$_TestItem get _value => super._value as _$_TestItem;

  @override
  $Res call({
    Object? label = freezed,
    Object? icon = freezed,
  }) {
    return _then(_$_TestItem(
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String,
      icon: icon == freezed
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as IconData,
    ));
  }
}

/// @nodoc

class _$_TestItem implements _TestItem {
  const _$_TestItem({required this.label, required this.icon});

  @override
  final String label;
  @override
  final IconData icon;

  @override
  String toString() {
    return 'TestItem(label: $label, icon: $icon)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TestItem &&
            const DeepCollectionEquality().equals(other.label, label) &&
            const DeepCollectionEquality().equals(other.icon, icon));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(label),
      const DeepCollectionEquality().hash(icon));

  @JsonKey(ignore: true)
  @override
  _$$_TestItemCopyWith<_$_TestItem> get copyWith =>
      __$$_TestItemCopyWithImpl<_$_TestItem>(this, _$identity);
}

abstract class _TestItem implements TestItem {
  const factory _TestItem(
      {required final String label,
      required final IconData icon}) = _$_TestItem;

  @override
  String get label;
  @override
  IconData get icon;
  @override
  @JsonKey(ignore: true)
  _$$_TestItemCopyWith<_$_TestItem> get copyWith =>
      throw _privateConstructorUsedError;
}
