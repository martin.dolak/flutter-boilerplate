import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

part 'person.freezed.dart';
part 'person.g.dart';

@freezed
class Person with _$Person {
  @HiveType(typeId: 1, adapterName: "PersonAdapter")
  const factory Person({
    @HiveField(0) required String name,
    @HiveField(1) required String email,
  }) = _Person;

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
}
