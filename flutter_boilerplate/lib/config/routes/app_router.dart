import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/presentation/core/screen/not_found_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/data/test_data_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/design/design_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/error/error_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/home/home_screen.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    return MaterialPageRoute(builder: (_) => _composeWidget(settings));
  }

  static Widget _composeWidget(RouteSettings settings) {
    switch (settings.name) {
      case HomeScreen.routeName:
        return const HomeScreen();
      case ErrorScreen.routeName:
        return const ErrorScreen();
      case DesignScreen.routeName:
        return const DesignScreen();
      case TestDataScreen.routeName:
        return const TestDataScreen();
      default:
        return NotFoundScreen(routeName: settings.name);
    }
  }
}
