// ignore_for_file: constant_identifier_names

class AppConfig {
  // DEV
  static const DEV_SERVER_BASE_URL = "http://10.0.2.2:8081";

  // STAG
  static const STAG_SERVER_BASE_URL = "http://stag-server.com";

  // PROD
  static const PROD_SERVER_BASE_URL = "http://prod-server.com";

  // GENERAL

  // HTTP client
  static const REQUEST_CONNECTION_TIMEOUT = 10 * 1000;
  static const REQUEST_RECEIVE_TIMEOUT = 10 * 1000;

  // Responsive size(s)
  static const MOBILE_WIDTH_MAX_SIZE = 800;
  static const TABLET_WIDTH_MAX_SIZE = 1200;
}
