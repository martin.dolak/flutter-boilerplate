import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/config/design/theme/app_colors.dart';

ThemeData lightTheme() {
  return ThemeData(
    brightness: Brightness.light,
    appBarTheme: const AppBarTheme(
      color: AppColors.LIGHT_PRIMARY_COLOR,
    ),
    primaryColor: AppColors.LIGHT_PRIMARY_COLOR,
    errorColor: Colors.red,
    scaffoldBackgroundColor: Colors.white,
    colorScheme: const ColorScheme.light(
      secondary: Colors.orange,
    ),
    backgroundColor: Colors.white,
  );
}

ThemeData darkTheme() {
  return ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColor: AppColors.DARK_PRIMARY_COLOR,
    errorColor: Colors.red,
    scaffoldBackgroundColor: Colors.grey.shade700,
    appBarTheme: const AppBarTheme(
      color: AppColors.DARK_PRIMARY_COLOR,
    ),
    colorScheme: ColorScheme.dark(
      secondary: Colors.lime.shade400,
    ),
    backgroundColor: Colors.grey.shade700,
  );
}
