// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

class AppColors {
  // light
  static const Color LIGHT_PRIMARY_COLOR = Color(0xff23aaff);

  // dark
  static const Color DARK_PRIMARY_COLOR = Color(0xff1a237e);

  // general
  static const Color ERROR_COLOR = Color(0xffff9900);
}
