import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/config/extension/build_context_ext.dart';

class ResponsievBuilder extends StatelessWidget {
  final Widget mobile;
  final Widget? tablet;
  final Widget? desktop;

  const ResponsievBuilder({
    required this.mobile,
    this.tablet,
    this.desktop,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (context.isMobile()) {
          return mobile;
        }

        if (context.isTablet()) {
          return tablet ?? mobile;
        }

        return desktop ?? tablet ?? mobile;
      },
    );
  }
}
