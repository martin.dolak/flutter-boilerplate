import 'package:flutter_boilerplate/config/common/flavor.dart';

class FlavorConfigHolder {
  final String serverUrl;
  final FLAVOR flavor;
  final bool isProd;

  static FlavorConfigHolder? _instance;

  FlavorConfigHolder._(this.serverUrl, this.flavor, this.isProd) {
    _instance = this;
  }

  FlavorConfigHolder.create({
    required String serverUrl,
    required FLAVOR flavor,
    required bool isProd,
  }) : this._(
          serverUrl,
          flavor,
          isProd,
        );

  static FlavorConfigHolder get inst {
    if (_instance == null) throw Exception("Cannot get null instance");

    return _instance!;
  }

  String get flavorName => flavor.name;
}
