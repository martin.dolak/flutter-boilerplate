import 'package:freezed_annotation/freezed_annotation.dart';

part 'error.freezed.dart';
part 'error.g.dart';

@JsonSerializable()
class ValidationResult {
  final String message;
  final String errorCode;

  ValidationResult({required this.message, required this.errorCode});

  factory ValidationResult.fromJson(Map<String, dynamic> json) =>
      _$ValidationResultFromJson(json);
  Map<String, dynamic> toJson() => _$ValidationResultToJson(this);
}

@freezed
class AppError with _$AppError implements Exception {
  const factory AppError.validationError(ValidationResult? result) =
      ValidationError;
  const factory AppError.uknownError(String? message) = UnknownError;
  const factory AppError.serverError() = ServerError;
  const factory AppError.notFoundError() = NotFoundError;
  const factory AppError.timeoutExceededError() = TimeoutExceededError;
  const factory AppError.badRequestError() = BadRequestError;
}
