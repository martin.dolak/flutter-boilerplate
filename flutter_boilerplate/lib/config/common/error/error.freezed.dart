// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'error.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AppError {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppErrorCopyWith<$Res> {
  factory $AppErrorCopyWith(AppError value, $Res Function(AppError) then) =
      _$AppErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$AppErrorCopyWithImpl<$Res> implements $AppErrorCopyWith<$Res> {
  _$AppErrorCopyWithImpl(this._value, this._then);

  final AppError _value;
  // ignore: unused_field
  final $Res Function(AppError) _then;
}

/// @nodoc
abstract class _$$ValidationErrorCopyWith<$Res> {
  factory _$$ValidationErrorCopyWith(
          _$ValidationError value, $Res Function(_$ValidationError) then) =
      __$$ValidationErrorCopyWithImpl<$Res>;
  $Res call({ValidationResult? result});
}

/// @nodoc
class __$$ValidationErrorCopyWithImpl<$Res> extends _$AppErrorCopyWithImpl<$Res>
    implements _$$ValidationErrorCopyWith<$Res> {
  __$$ValidationErrorCopyWithImpl(
      _$ValidationError _value, $Res Function(_$ValidationError) _then)
      : super(_value, (v) => _then(v as _$ValidationError));

  @override
  _$ValidationError get _value => super._value as _$ValidationError;

  @override
  $Res call({
    Object? result = freezed,
  }) {
    return _then(_$ValidationError(
      result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as ValidationResult?,
    ));
  }
}

/// @nodoc

class _$ValidationError implements ValidationError {
  const _$ValidationError(this.result);

  @override
  final ValidationResult? result;

  @override
  String toString() {
    return 'AppError.validationError(result: $result)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ValidationError &&
            const DeepCollectionEquality().equals(other.result, result));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(result));

  @JsonKey(ignore: true)
  @override
  _$$ValidationErrorCopyWith<_$ValidationError> get copyWith =>
      __$$ValidationErrorCopyWithImpl<_$ValidationError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) {
    return validationError(result);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) {
    return validationError?.call(result);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) {
    if (validationError != null) {
      return validationError(result);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) {
    return validationError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) {
    return validationError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) {
    if (validationError != null) {
      return validationError(this);
    }
    return orElse();
  }
}

abstract class ValidationError implements AppError {
  const factory ValidationError(final ValidationResult? result) =
      _$ValidationError;

  ValidationResult? get result;
  @JsonKey(ignore: true)
  _$$ValidationErrorCopyWith<_$ValidationError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnknownErrorCopyWith<$Res> {
  factory _$$UnknownErrorCopyWith(
          _$UnknownError value, $Res Function(_$UnknownError) then) =
      __$$UnknownErrorCopyWithImpl<$Res>;
  $Res call({String? message});
}

/// @nodoc
class __$$UnknownErrorCopyWithImpl<$Res> extends _$AppErrorCopyWithImpl<$Res>
    implements _$$UnknownErrorCopyWith<$Res> {
  __$$UnknownErrorCopyWithImpl(
      _$UnknownError _value, $Res Function(_$UnknownError) _then)
      : super(_value, (v) => _then(v as _$UnknownError));

  @override
  _$UnknownError get _value => super._value as _$UnknownError;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$UnknownError(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$UnknownError implements UnknownError {
  const _$UnknownError(this.message);

  @override
  final String? message;

  @override
  String toString() {
    return 'AppError.uknownError(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownError &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$UnknownErrorCopyWith<_$UnknownError> get copyWith =>
      __$$UnknownErrorCopyWithImpl<_$UnknownError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) {
    return uknownError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) {
    return uknownError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) {
    if (uknownError != null) {
      return uknownError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) {
    return uknownError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) {
    return uknownError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) {
    if (uknownError != null) {
      return uknownError(this);
    }
    return orElse();
  }
}

abstract class UnknownError implements AppError {
  const factory UnknownError(final String? message) = _$UnknownError;

  String? get message;
  @JsonKey(ignore: true)
  _$$UnknownErrorCopyWith<_$UnknownError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ServerErrorCopyWith<$Res> {
  factory _$$ServerErrorCopyWith(
          _$ServerError value, $Res Function(_$ServerError) then) =
      __$$ServerErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ServerErrorCopyWithImpl<$Res> extends _$AppErrorCopyWithImpl<$Res>
    implements _$$ServerErrorCopyWith<$Res> {
  __$$ServerErrorCopyWithImpl(
      _$ServerError _value, $Res Function(_$ServerError) _then)
      : super(_value, (v) => _then(v as _$ServerError));

  @override
  _$ServerError get _value => super._value as _$ServerError;
}

/// @nodoc

class _$ServerError implements ServerError {
  const _$ServerError();

  @override
  String toString() {
    return 'AppError.serverError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ServerError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) {
    return serverError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) {
    return serverError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) {
    return serverError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) {
    return serverError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) {
    if (serverError != null) {
      return serverError(this);
    }
    return orElse();
  }
}

abstract class ServerError implements AppError {
  const factory ServerError() = _$ServerError;
}

/// @nodoc
abstract class _$$NotFoundErrorCopyWith<$Res> {
  factory _$$NotFoundErrorCopyWith(
          _$NotFoundError value, $Res Function(_$NotFoundError) then) =
      __$$NotFoundErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NotFoundErrorCopyWithImpl<$Res> extends _$AppErrorCopyWithImpl<$Res>
    implements _$$NotFoundErrorCopyWith<$Res> {
  __$$NotFoundErrorCopyWithImpl(
      _$NotFoundError _value, $Res Function(_$NotFoundError) _then)
      : super(_value, (v) => _then(v as _$NotFoundError));

  @override
  _$NotFoundError get _value => super._value as _$NotFoundError;
}

/// @nodoc

class _$NotFoundError implements NotFoundError {
  const _$NotFoundError();

  @override
  String toString() {
    return 'AppError.notFoundError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NotFoundError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) {
    return notFoundError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) {
    return notFoundError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) {
    if (notFoundError != null) {
      return notFoundError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) {
    return notFoundError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) {
    return notFoundError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) {
    if (notFoundError != null) {
      return notFoundError(this);
    }
    return orElse();
  }
}

abstract class NotFoundError implements AppError {
  const factory NotFoundError() = _$NotFoundError;
}

/// @nodoc
abstract class _$$TimeoutExceededErrorCopyWith<$Res> {
  factory _$$TimeoutExceededErrorCopyWith(_$TimeoutExceededError value,
          $Res Function(_$TimeoutExceededError) then) =
      __$$TimeoutExceededErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$TimeoutExceededErrorCopyWithImpl<$Res>
    extends _$AppErrorCopyWithImpl<$Res>
    implements _$$TimeoutExceededErrorCopyWith<$Res> {
  __$$TimeoutExceededErrorCopyWithImpl(_$TimeoutExceededError _value,
      $Res Function(_$TimeoutExceededError) _then)
      : super(_value, (v) => _then(v as _$TimeoutExceededError));

  @override
  _$TimeoutExceededError get _value => super._value as _$TimeoutExceededError;
}

/// @nodoc

class _$TimeoutExceededError implements TimeoutExceededError {
  const _$TimeoutExceededError();

  @override
  String toString() {
    return 'AppError.timeoutExceededError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$TimeoutExceededError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) {
    return timeoutExceededError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) {
    return timeoutExceededError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) {
    if (timeoutExceededError != null) {
      return timeoutExceededError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) {
    return timeoutExceededError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) {
    return timeoutExceededError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) {
    if (timeoutExceededError != null) {
      return timeoutExceededError(this);
    }
    return orElse();
  }
}

abstract class TimeoutExceededError implements AppError {
  const factory TimeoutExceededError() = _$TimeoutExceededError;
}

/// @nodoc
abstract class _$$BadRequestErrorCopyWith<$Res> {
  factory _$$BadRequestErrorCopyWith(
          _$BadRequestError value, $Res Function(_$BadRequestError) then) =
      __$$BadRequestErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BadRequestErrorCopyWithImpl<$Res> extends _$AppErrorCopyWithImpl<$Res>
    implements _$$BadRequestErrorCopyWith<$Res> {
  __$$BadRequestErrorCopyWithImpl(
      _$BadRequestError _value, $Res Function(_$BadRequestError) _then)
      : super(_value, (v) => _then(v as _$BadRequestError));

  @override
  _$BadRequestError get _value => super._value as _$BadRequestError;
}

/// @nodoc

class _$BadRequestError implements BadRequestError {
  const _$BadRequestError();

  @override
  String toString() {
    return 'AppError.badRequestError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BadRequestError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValidationResult? result) validationError,
    required TResult Function(String? message) uknownError,
    required TResult Function() serverError,
    required TResult Function() notFoundError,
    required TResult Function() timeoutExceededError,
    required TResult Function() badRequestError,
  }) {
    return badRequestError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
  }) {
    return badRequestError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValidationResult? result)? validationError,
    TResult Function(String? message)? uknownError,
    TResult Function()? serverError,
    TResult Function()? notFoundError,
    TResult Function()? timeoutExceededError,
    TResult Function()? badRequestError,
    required TResult orElse(),
  }) {
    if (badRequestError != null) {
      return badRequestError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ValidationError value) validationError,
    required TResult Function(UnknownError value) uknownError,
    required TResult Function(ServerError value) serverError,
    required TResult Function(NotFoundError value) notFoundError,
    required TResult Function(TimeoutExceededError value) timeoutExceededError,
    required TResult Function(BadRequestError value) badRequestError,
  }) {
    return badRequestError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
  }) {
    return badRequestError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ValidationError value)? validationError,
    TResult Function(UnknownError value)? uknownError,
    TResult Function(ServerError value)? serverError,
    TResult Function(NotFoundError value)? notFoundError,
    TResult Function(TimeoutExceededError value)? timeoutExceededError,
    TResult Function(BadRequestError value)? badRequestError,
    required TResult orElse(),
  }) {
    if (badRequestError != null) {
      return badRequestError(this);
    }
    return orElse();
  }
}

abstract class BadRequestError implements AppError {
  const factory BadRequestError() = _$BadRequestError;
}
