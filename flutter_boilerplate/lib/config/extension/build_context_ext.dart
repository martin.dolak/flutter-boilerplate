import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/config/app_config.dart';

extension BuildContextExt on BuildContext {
  bool isMobile() =>
      MediaQuery.of(this).size.shortestSide <= AppConfig.MOBILE_WIDTH_MAX_SIZE;

  bool isTablet() {
    var shortestSide = MediaQuery.of(this).size.shortestSide;
    return shortestSide > AppConfig.MOBILE_WIDTH_MAX_SIZE &&
        shortestSide <= AppConfig.TABLET_WIDTH_MAX_SIZE;
  }

  bool isDesktop() =>
      MediaQuery.of(this).size.shortestSide > AppConfig.TABLET_WIDTH_MAX_SIZE;
}
