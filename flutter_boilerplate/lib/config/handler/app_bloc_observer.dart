import 'package:bloc/bloc.dart';
import 'package:flutter_boilerplate/config/handler/error_handler.dart';

class AppBlocObserver extends BlocObserver {
  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    ErrorHandler.handleThirdPartyException(error, stackTrace);
  }
}
