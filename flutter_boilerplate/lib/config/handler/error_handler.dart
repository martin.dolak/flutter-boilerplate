import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/config/common/error/error.dart';
import 'package:flutter_boilerplate/presentation/app.dart';

class ErrorHandler {
  static handleThrownException(FlutterErrorDetails details) {
    FlutterError.presentError(details);
    _showSnackBar(details.exceptionAsString());
  }

  static handleThirdPartyException(Object error, StackTrace stackTrace) {
    FlutterError.presentError(
        FlutterErrorDetails(exception: error, stack: stackTrace));
    if (error is AppError) {
      String message = error.when(
        validationError: (_) => "Validation Exception",
        uknownError: (m) => "Unknown Exception: $m",
        serverError: () => "Internal Server Expcetion",
        notFoundError: () => "Not Found Expcetion",
        timeoutExceededError: () => "Timeout Exceeded Expcetion",
        badRequestError: () => "Bad Request Expcetion",
      );

      _showSnackBar(message);
      return;
    }

    _showSnackBar(error.toString());
  }

  static AppError handleDioError(DioError error) {
    switch (error.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw const TimeoutExceededError();
      case DioErrorType.other:
      case DioErrorType.cancel:
        break;
      case DioErrorType.response:
        switch (error.response?.statusCode) {
          case 404:
            throw const NotFoundError();
          case 500:
            throw const ServerError();
          case 400:
            var result = ValidationResult.fromJson(error.response?.data);
            return ValidationError(result);
        }
        break;
    }
    throw UnknownError(error.message);
  }

  static void _showSnackBar(String text) {
    App.snackbarKey.currentState?.hideCurrentSnackBar();
    App.snackbarKey.currentState?.showSnackBar(_createSnackBar(text));
  }

  static SnackBar _createSnackBar(String text) {
    return SnackBar(
      content: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.red,
      duration: const Duration(seconds: 5),
      behavior: SnackBarBehavior.floating,
    );
  }
}
