import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'package:flutter_boilerplate/config/di/injector.config.dart';

final getIt = GetIt.instance;

@InjectableInit()
void configureDependencies(String environment) =>
    $initGetIt(getIt, environment: environment);
