// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:connectivity_plus/connectivity_plus.dart' as _i6;
import 'package:get_it/get_it.dart' as _i1;
import 'package:hive/hive.dart' as _i5;
import 'package:injectable/injectable.dart' as _i2;

import '../../data/repository/auth/auth_repository.dart' as _i4;
import '../../data/repository/data/test_data_repository.dart' as _i12;
import '../../data/store/auth/auth_bloc.dart' as _i11;
import '../../data/store/data/test_data_bloc.dart' as _i13;
import '../../data/store/design/design_cubit.dart' as _i7;
import '../../data/store/error/throw_error_cubit.dart' as _i9;
import '../../db/app_test_data_storage.dart' as _i10;
import '../../service/notification_service.dart' as _i8;
import '../http/app_http_client.dart' as _i3;
import 'db_module.dart' as _i14;
import 'network_module.dart' as _i15; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final dbModule = _$DbModule();
  final networkModule = _$NetworkModule();
  gh.singleton<_i3.AppHttpClint>(_i3.AppHttpClint.createClient());
  gh.singleton<_i4.AuthRepository>(
      _i4.AuthRepositoryImpl(http: get<_i3.AppHttpClint>()));
  gh.singleton<_i5.Box<dynamic>>(dbModule.testDataBox,
      instanceName: 'AppTestDataBox');
  gh.lazySingleton<_i6.Connectivity>(() => networkModule.connectovity);
  gh.factory<_i7.DesignCubit>(() => _i7.DesignCubit());
  gh.lazySingleton<_i8.NotificationService>(() => _i8.NotificationService());
  gh.factory<_i9.ThrowErrorCubit>(() => _i9.ThrowErrorCubit());
  gh.lazySingleton<_i10.AppTestDataStorage>(() => _i10.AppTestDataStorage(
      get<_i5.Box<dynamic>>(instanceName: 'AppTestDataBox')));
  gh.factory<_i11.AuthBloc>(
      () => _i11.AuthBloc(authRepository: get<_i4.AuthRepository>()));
  gh.lazySingleton<_i12.TestDataRepository>(() => _i12.TestDataRepositoryImpl(
      http: get<_i3.AppHttpClint>(), storage: get<_i10.AppTestDataStorage>()));
  gh.factory<_i13.TestDataBloc>(() =>
      _i13.TestDataBloc(testDataRepository: get<_i12.TestDataRepository>()));
  return get;
}

class _$DbModule extends _i14.DbModule {}

class _$NetworkModule extends _i15.NetworkModule {}
