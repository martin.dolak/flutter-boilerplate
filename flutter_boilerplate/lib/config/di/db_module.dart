import 'package:flutter_boilerplate/data/model/data/person.dart';
import 'package:flutter_boilerplate/db/constants.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

@module
abstract class DbModule {
  @singleton
  @Named(Constants.APP_TEST_DATA_BOX)
  Box get testDataBox => Hive.box<Person>(Constants.APP_TEST_DATA_BOX);
}
