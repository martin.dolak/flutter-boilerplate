class Result<R, E> {
  E? _error;
  R? _result;

  Result.success(R result) : _result = result;
  Result.error(E error) : _error = error;

  void when({required Function(R) success, required Function(E) error}) {
    if (_result != null) {
      success(_result!);
    } else {
      error(_error!);
    }
  }
}
