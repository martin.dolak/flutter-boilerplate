import 'package:dio/native_imp.dart';
import 'package:flutter_boilerplate/config/app_config.dart';
import 'package:flutter_boilerplate/config/common/flavor_config_holder.dart';
import 'package:injectable/injectable.dart';

@singleton
class AppHttpClint extends DioForNative {
  @factoryMethod
  static AppHttpClint createClient() {
    return AppHttpClint()
      ..options.connectTimeout = AppConfig.REQUEST_CONNECTION_TIMEOUT
      ..options.receiveTimeout = AppConfig.REQUEST_RECEIVE_TIMEOUT
      ..options.baseUrl = FlavorConfigHolder.inst.serverUrl
      ..interceptors.addAll(
        [],
      );
  }
}
