import 'package:flutter_boilerplate/config/app_config.dart';
import 'package:flutter_boilerplate/config/common/flavor.dart';
import 'package:flutter_boilerplate/config/common/flavor_config_holder.dart';
import 'package:flutter_boilerplate/presentation/app.dart';

void main() {
  FlavorConfigHolder.create(
    flavor: FLAVOR.dev,
    serverUrl: AppConfig.DEV_SERVER_BASE_URL,
    isProd: false,
  );

  runApplication();
}
