import 'package:flutter_boilerplate/data/model/data/person.dart';
import 'package:flutter_boilerplate/db/constants.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class AppTestDataStorage {
  final Box _dataStorage;

  AppTestDataStorage(
    @Named(Constants.APP_TEST_DATA_BOX) final Box dataStorage,
  ) : _dataStorage = dataStorage;

  Future<void> refreshPersons(List<Person> persons) async {
    await _dataStorage.clear();
    await _dataStorage.addAll(persons);
  }
}
