import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/config/common/flavor_config_holder.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Boilerplate (${FlavorConfigHolder.inst.flavorName})",
              style: const TextStyle(
                color: Colors.blue,
                fontSize: 30,
              ),
              textAlign: TextAlign.center,
            ),
            const Text("Splash screen"),
          ],
        ),
      ),
    );
  }
}
