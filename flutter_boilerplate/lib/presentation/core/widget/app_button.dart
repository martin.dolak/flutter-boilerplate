import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final String label;
  final VoidCallback press;
  final double width;
  final double heigth;

  const AppButton({
    required this.label,
    required this.press,
    this.width = double.infinity,
    this.heigth = 80,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = OutlinedButton.styleFrom(
      textStyle: const TextStyle(
        fontSize: 20,
      ),
      backgroundColor: Colors.grey.shade300,
      primary: Colors.black87,
    );

    return SizedBox(
      width: width,
      height: heigth,
      child: OutlinedButton(
        style: style,
        child: Text(label),
        onPressed: press,
      ),
    );
  }
}
