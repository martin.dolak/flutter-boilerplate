import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/data/model/data/person.dart';
import 'package:flutter_boilerplate/data/store/data/test_data_bloc.dart';
import 'package:flutter_boilerplate/db/constants.dart';
import 'package:flutter_boilerplate/presentation/core/widget/app_button.dart';
import 'package:hive_flutter/hive_flutter.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<TestDataBloc>().add(const TestDataEvent.refresh());
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: BlocBuilder<TestDataBloc, TestDataState>(
            builder: (context, state) {
              return state.maybeWhen(
                loading: () => const CircularProgressIndicator(),
                orElse: () => AppButton(
                  label: "Refresh Data",
                  press: () {
                    context
                        .read<TestDataBloc>()
                        .add(const TestDataEvent.refresh());
                  },
                ),
              );
            },
          ),
        ),
        ValueListenableBuilder(
          valueListenable:
              Hive.box<Person>(Constants.APP_TEST_DATA_BOX).listenable(),
          builder: (context, Box<Person> box, child) {
            if (box.isEmpty) {
              return const Text("No Persons");
            }
            return Expanded(
              child: ListView.builder(
                itemCount: box.values.length,
                itemBuilder: (context, index) {
                  Person person = box.getAt(index)!;
                  return ListTile(
                    onTap: () {},
                    leading: const Icon(Icons.person),
                    title: Text(person.name),
                    trailing: Text(person.email),
                  );
                },
              ),
            );
          },
        )
      ],
    );
  }
}
