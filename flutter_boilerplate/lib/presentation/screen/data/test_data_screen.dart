import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/config/di/injector.dart';
import 'package:flutter_boilerplate/data/store/data/test_data_bloc.dart';
import 'package:flutter_boilerplate/presentation/screen/data/widgets/body.dart';

class TestDataScreen extends StatelessWidget {
  static const String routeName = "/data";

  const TestDataScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Test Data Screen"),
      ),
      body: BlocProvider<TestDataBloc>(
        create: (context) => getIt<TestDataBloc>(),
        child: const Body(),
      ),
    );
  }
}
