import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/data/store/auth/auth_bloc.dart';
import 'package:flutter_boilerplate/data/store/error/throw_error_cubit.dart';
import 'package:flutter_boilerplate/presentation/core/widget/app_button.dart';

class ErrorScreen extends StatelessWidget {
  static const String routeName = "/error-page";

  const ErrorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _showErrorText(String text) {
      return Text(
        text,
        style: const TextStyle(color: Colors.red),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Error Screen"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          child: Column(
            children: [
              BlocBuilder<ThrowErrorCubit, ThrowErrorState>(
                builder: (context, state) {
                  return state.when(
                    init: () => const SizedBox.shrink(),
                    error: (e) => _showErrorText(e),
                  );
                },
              ),
              const SizedBox(
                height: 10,
              ),
              AppButton(
                label: "Throw Error",
                press: () => context.read<ThrowErrorCubit>().throwException(),
              ),
              const SizedBox(
                height: 10,
              ),
              AppButton(
                label: "Handle Error",
                press: () => context.read<ThrowErrorCubit>().handleException(),
              ),
              const SizedBox(
                height: 10,
              ),
              BlocBuilder<AuthBloc, AuthState>(
                builder: (context, state) {
                  return state.maybeWhen(
                    error: (e) {
                      return e.maybeWhen(
                        validationError: (e) =>
                            _showErrorText("${e?.errorCode}:${e?.message}"),
                        orElse: () => const SizedBox.shrink(),
                      );
                    },
                    orElse: () => const SizedBox.shrink(),
                  );
                },
              ),
              AppButton(
                label: "Validation Error",
                press: () => context.read<AuthBloc>().add(
                      const AuthEvent.validationError(),
                    ),
              ),
              const SizedBox(
                height: 10,
              ),
              AppButton(
                label: "Add error in Cubit",
                press: () => context.read<ThrowErrorCubit>().addException(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
