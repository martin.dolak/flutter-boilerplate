import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/config/common/flavor_config_holder.dart';
import 'package:flutter_boilerplate/presentation/screen/home/widgets/body.dart';
import 'package:flutter_boilerplate/service/notification_service.dart';
import 'package:flutter_boilerplate/config/di/injector.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = "/";

  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var ns = getIt<NotificationService>();

  @override
  void initState() {
    super.initState();
    ns.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Boilerplate (${FlavorConfigHolder.inst.flavorName})"),
      ),
      body: const Body(),
    );
  }
}
