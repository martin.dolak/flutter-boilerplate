import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/presentation/core/widget/app_button.dart';
import 'package:flutter_boilerplate/presentation/screen/data/test_data_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/design/design_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/error/error_screen.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        child: Column(
          children: [
            AppButton(
              label: "Error Screen",
              press: () =>
                  Navigator.of(context).pushNamed(ErrorScreen.routeName),
            ),
            const SizedBox(
              height: 10,
            ),
            AppButton(
              label: "Design screen",
              press: () =>
                  Navigator.of(context).pushNamed(DesignScreen.routeName),
            ),
            const SizedBox(
              height: 10,
            ),
            AppButton(
              label: "Data screen",
              press: () =>
                  Navigator.of(context).pushNamed(TestDataScreen.routeName),
            ),
          ],
        ),
      ),
    );
  }
}
