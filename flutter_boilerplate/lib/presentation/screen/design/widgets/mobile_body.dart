import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/data/model/design/test_item.dart';

class MobileBody extends StatelessWidget {
  const MobileBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        TestItem item = list[index];
        return ListTile(
          onTap: () {},
          leading: Icon(item.icon),
          title: Text(item.label),
        );
      },
      itemCount: list.length,
    );
  }
}
