import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/data/model/design/test_item.dart';

class TabletBody extends StatelessWidget {
  const TabletBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
        ),
        itemBuilder: (context, index) {
          TestItem item = list[index];
          return InkWell(
            onTap: () {},
            child: GridTile(
              header: Text(
                item.label,
                textAlign: TextAlign.center,
              ),
              child: Icon(item.icon),
            ),
          );
        },
        itemCount: list.length,
      ),
    );
  }
}
