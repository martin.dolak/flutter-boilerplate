import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/config/design/responsive_builder.dart';
import 'package:flutter_boilerplate/data/store/design/design_cubit.dart';
import 'package:flutter_boilerplate/presentation/screen/design/widgets/mobile_body.dart';
import 'package:flutter_boilerplate/presentation/screen/design/widgets/tablet_body.dart';

class DesignScreen extends StatelessWidget {
  static const String routeName = "/design";

  const DesignScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Design screen"),
        actions: [
          IconButton(
            icon: BlocBuilder<DesignCubit, DesignState>(
              builder: (context, state) {
                var iconData = state.when(
                    light: () => Icons.wb_sunny_outlined,
                    dark: () => Icons.nightlight_outlined);
                return Icon(iconData);
              },
            ),
            onPressed: () => context.read<DesignCubit>().changeTheme(),
          ),
        ],
      ),
      body: const ResponsievBuilder(
        mobile: MobileBody(),
        tablet: TabletBody(),
      ),
    );
  }
}
