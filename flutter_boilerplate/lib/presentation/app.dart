import 'dart:async';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/config/common/flavor_config_holder.dart';
import 'package:flutter_boilerplate/config/design/theme/app_theme.dart';
import 'package:flutter_boilerplate/config/di/injector.dart';
import 'package:flutter_boilerplate/config/handler/app_bloc_observer.dart';
import 'package:flutter_boilerplate/config/handler/error_handler.dart';
import 'package:flutter_boilerplate/config/routes/app_router.dart';
import 'package:flutter_boilerplate/data/model/data/person.dart';
import 'package:flutter_boilerplate/data/store/auth/auth_bloc.dart';
import 'package:flutter_boilerplate/data/store/design/design_cubit.dart';
import 'package:flutter_boilerplate/data/store/error/throw_error_cubit.dart';
import 'package:flutter_boilerplate/db/constants.dart';
import 'package:flutter_boilerplate/presentation/core/screen/splash_screen.dart';
import 'package:flutter_boilerplate/presentation/screen/home/home_screen.dart';
import 'package:hive_flutter/hive_flutter.dart';

runApplication() {
  runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();

    Bloc.observer = AppBlocObserver(); // Bloc configuration
    await Firebase.initializeApp();
    await _initStorage(); // init Local storage

    // should be last
    configureDependencies(
        FlavorConfigHolder.inst.flavorName); // DI configuration

    FlutterError.onError = (details) {
      ErrorHandler.handleThrownException(details);
      if (FlavorConfigHolder.inst.isProd) exit(1);
    };

    runApp(
      MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>(
            create: (context) =>
                getIt<AuthBloc>()..add(const AuthEvent.started()),
          ),
          BlocProvider<ThrowErrorCubit>(
            create: (context) => getIt<ThrowErrorCubit>(),
          ),
          BlocProvider<DesignCubit>(
            create: (context) => getIt<DesignCubit>(),
          ),
        ],
        child: const App(),
      ),
    );
  }, (error, stack) {
    ErrorHandler.handleThirdPartyException(error, stack);
    if (FlavorConfigHolder.inst.isProd) exit(1);
  });
}

Future<void> _initStorage() async {
  await Hive.initFlutter();
  Hive.registerAdapter(PersonAdapter());
  await Hive.openBox<Person>(Constants.APP_TEST_DATA_BOX);
}

class App extends StatelessWidget {
  static GlobalKey<ScaffoldMessengerState> snackbarKey =
      GlobalKey<ScaffoldMessengerState>();

  const App({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DesignCubit, DesignState>(
      builder: (context, state) => MaterialApp(
        title: "Flutter Boilerplate",
        debugShowCheckedModeBanner: false,
        onGenerateRoute: AppRouter.generateRoute,
        scaffoldMessengerKey: snackbarKey,
        theme: state.when(
          light: () => lightTheme(),
          dark: () => darkTheme(),
        ),
        themeMode: ThemeMode.system,
        home: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            return state.maybeWhen(
              authorized: () => const HomeScreen(),
              error: (err) => const HomeScreen(),
              orElse: () => const SplashScreen(),
            );
          },
        ),
      ),
    );
  }
}
